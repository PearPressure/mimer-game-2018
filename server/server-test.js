const path = require('path');
const fs = require('fs');

const express =  require('express');
const app = express();

const serveIndex = require('serve-index')
const moment = require('moment');

console.log("hej");

const game_path = path.join(__dirname, '../games');
app.use("/games", express.static(game_path), serveIndex(game_path, {'icons': true}));
app.use("/lib", express.static(path.join(__dirname, '../lib')));
app.use("/assets", express.static(path.join(__dirname, '../assets')));
app.use("/",  function(req, res) {res.redirect('/games')});

console.log("http://localhost:"+process.argv[2]);
app.listen(process.argv[2]);

